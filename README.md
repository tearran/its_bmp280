
## Introduction to its-bmp280
include this script for the bmp280 a Python script that reads temperature and pressure values from a BMP280 sensor and outputs the results in either Celsius, Fahrenheit, or hectopascal units.

## Prerequisites
- Python 3
- [bosch bmp280](https://www.bosch-sensortec.com/products/environmental-sensors/pressure-sensors/bmp280/)
- feyzikesim - [bmp280](https://github.com/feyzikesim/bmp280) - Temprature and pressure Sensor

## Usage
To run the script, execute the following command in the terminal:

$` python its_bmp280.py [-c|-f|-p|-hpa|-h --help] `

#### Results
```
Usage: filename -option
-h or --help: display this help
-f: Fahrenheit
-c: Celsius
-p: or -hPa for Pressure
```

## Support
Join us on [Discord](https://discord.com/invite/MENHMuTmyH)

## Authors and acknowledgment
- drop-in libraries 
    - feyzikesim - [bmp280](https://github.com/feyzikesim/bmp280) - Temprature and pressure Sensor
    - kplindegaard - [smbus2](https://github.com/kplindegaard/smbus2) - Drop-in replacement of SMBus

## License
GNU General Public License v2.0


## Project status
Testing
